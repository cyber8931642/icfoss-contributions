package main

import (
    "database/sql"
    "fmt"
    _ "github.com/lib/pq"
    "net/http"
    "html/template"
)

func main() {
    // Define PostgreSQL connection parameters
    dbHost := "localhost"
    dbPort := 5432
    dbName := "postgres"
    dbUser := "postgres"
    dbPassword := "password"

    // Create the connection string
    connectionStr := fmt.Sprintf(
        "host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
        dbHost, dbPort, dbUser, dbPassword, dbName,
    )

    // Open a connection to the database
    db, err := sql.Open("postgres", connectionStr)
    if err != nil {
        panic(err)
    }
    defer db.Close()

    // Verify the connection
    err = db.Ping()
    if err != nil {
        panic(err)
    }

    fmt.Println("Successfully connected to the PostgreSQL database!")

    // You can now perform database operations with the 'db' connection.
    // Example: querying data, inserting data, updating data, etc.
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		
        if r.Method == http.MethodGet {
            tmpl := template.Must(template.ParseFiles("index.html"))
            tmpl.Execute(w, nil)
        }
    })

    http.HandleFunc("/sayhello", func(w http.ResponseWriter, r *http.Request) {
        if r.Method == http.MethodGet {
            w.Write([]byte("hello"))
        }
    })

    http.ListenAndServe(":3001", nil)
}
