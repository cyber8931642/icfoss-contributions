package main

import (
    "net/http"
    "html/template"
)

func main() {
    http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		
        if r.Method == http.MethodGet {
            tmpl := template.Must(template.ParseFiles("index.html"))
            tmpl.Execute(w, nil)
        }
    })

    http.HandleFunc("/sayhello", func(w http.ResponseWriter, r *http.Request) {
        if r.Method == http.MethodGet {
            w.Write([]byte("hello"))
        }
    })

    http.ListenAndServe(":3000", nil)
}
