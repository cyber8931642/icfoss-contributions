package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"
	_ "github.com/lib/pq"
	
)

// Database configuration
const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "password"
	dbname   = "login_history"
)

type Person struct {
	ID    int
	Name  string
	Email string
}

func main() {
	// Connect to the PostgreSQL database
	connStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Create a table for storing login data
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS user_login_history (
        id serial PRIMARY KEY,
        username VARCHAR(50) NOT NULL,
        password VARCHAR(50) NOT NULL,
        client_ip VARCHAR(15)
    )`)
	if err != nil {
		log.Fatal(err)
	}

	// Create an HTTP server
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "index.html")
	})

	http.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
			return
		}

		// Parse form data
		err := r.ParseForm()
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// Extract login data
		username := r.FormValue("username")
		password := r.FormValue("password")

		// Get the client's IP address
		clientIP := r.RemoteAddr

		// Insert the login data along with the client's IP into the database
		_, err = db.Exec("INSERT INTO user_login_history (username, password, client_ip) VALUES ($1, $2, $3)", username, password, clientIP)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "Login data with IP address written to the database.")
		
	})

	http.HandleFunc("/table", func(w http.ResponseWriter, r *http.Request) {
		// Fetch People data from the database
		rows, err := db.Query("SELECT id, name, email FROM your_table") // Replace with your actual table name
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer rows.Close()

		// Populate the data.People slice with fetched records
		var people []Person
		for rows.Next() {
			var person Person
			err := rows.Scan(&person.ID, &person.Name, &person.Email)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			people = append(people, person)
		}

		// Create a data struct for the HTML template
		data := struct {
			ClientIP string
			People   []Person
		}{
			ClientIP: r.RemoteAddr, // Get the client's IP address from the request
			People:   people,
		}

		tmpl, err := template.ParseFiles("login.html")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		tmpl.Execute(w, data) // Pass the data struct to the HTML template
	})

	// Start the HTTP server on port 3001
	log.Println("Server is running on :3001")
	http.ListenAndServe(":3001", nil)
}
