package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"net/http"

	_ "github.com/lib/pq"
)

// Database configuration
const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "password"
	dbname   = "login_history"
)

func main() {
	// Connect to the PostgreSQL database
	connStr := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Create a table for storing login data
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS user_login_history (
        id serial PRIMARY KEY,
        username VARCHAR(50) NOT NULL,
        password VARCHAR(50) NOT NULL
    )`)
	if err != nil {
		log.Fatal(err)
	}

	// Create an HTTP server
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "login.html")
	})

	http.HandleFunc("/login", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			http.Error(w, "Invalid request method", http.StatusMethodNotAllowed)
			return
		}

		// Parse form data
		err := r.ParseForm()
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// Extract login data
		username := r.FormValue("username")
		password := r.FormValue("password")

		// Insert the login data into the database
		_, err = db.Exec("INSERT INTO user_login_history (username, password) VALUES ($1, $2)", username, password)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		// Retrieve login data from the database
		rows, err := db.Query("SELECT id, username, password FROM user_login_history")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		defer rows.Close()

		var loginData []LoginEntry
		for rows.Next() {
			var entry LoginEntry
			err := rows.Scan(&entry.ID, &entry.Username, &entry.Password)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			loginData = append(loginData, entry)
		}

		// Display the login data in the table page (table.html)
		tmpl, err := template.ParseFiles("table.html")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		tmpl.Execute(w, loginData)
	})

	// Start the HTTP server on port 3001
	log.Println("Server is running on :3001")
	http.ListenAndServe(":3001", nil)
}

// LoginEntry represents a login entry in the database
type LoginEntry struct {
	ID       int
	Username string
	Password string
}
