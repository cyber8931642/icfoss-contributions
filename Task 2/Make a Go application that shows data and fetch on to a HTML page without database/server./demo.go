package main

import (
	"html/template"
	"log"
	"net/http"
)

// Define a struct to match the data structure
type Person struct {
	Name  string
	Email string
}

func main() {
	// Define data directly in the code
	data := []Person{
		{Name: "alwin", Email: "alwin@example.com"},
		{Name: "arun", Email: "arun@example.com"},
	}

	// Read the HTML template from a file
	tmpl, err := template.ParseFiles("index.html")
	if err != nil {
		log.Fatalf("Error parsing HTML template: %v", err)
	}

	// Create an HTTP server
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Execute the template with the data and send the result as a response
		if err := tmpl.Execute(w, data); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	})

	// Start the HTTP server on port 3000
	log.Println("Server is running on :3000")
	http.ListenAndServe(":3000", nil)
}
